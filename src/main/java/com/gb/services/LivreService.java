package com.gb.services;

import java.util.ArrayList;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gb.models.Livre;
import com.gb.repositories.LivreRepository;

@Service
public class LivreService {

	@Autowired  
	LivreRepository livreRepository;
	
	//get all
	public List<Livre> getAllLivre()   
	{  
	List<Livre> livres = new ArrayList<Livre>();  
	livreRepository.findAll().forEach(livre1 -> livres.add(livre1));  
	return livres;  
	} 
	
	//get one 
	public Livre getLivreById(String numLivre)   
	{  
	return livreRepository.findById(numLivre).get();  
	} 
	
	//save 
	public void save(Livre livre)   
	{  
	livreRepository.save(livre);  
	} 
	
	//delete
	public void delete(String numLivre)   
	{  
	livreRepository.deleteById(numLivre);  
	} 
	
	//update
	public Livre update(Livre livre, String numLivre)
    {
        Livre liv = livreRepository.findById(numLivre).get();
 
        if (Objects.nonNull(livre.getDesignation()) && !"".equalsIgnoreCase(livre.getDesignation())) {
            liv.setDesignation(livre.getDesignation());
        }
        
        if (Objects.nonNull(livre.getAuteur()) && !"".equalsIgnoreCase(livre.getAuteur())) {
            liv.setAuteur(livre.getAuteur());
        }
        
        if (Objects.nonNull(livre.getDateEdition()) && !"".equalsIgnoreCase(livre.getDateEdition())) {
            liv.setDateEdition(livre.getDateEdition());
        }
        
        if (Objects.nonNull(livre.getDisponible()) && !"".equalsIgnoreCase(livre.getDisponible())) {
            liv.setDisponible(livre.getDisponible());
        }
 
        return livreRepository.save(liv);
    }
}
