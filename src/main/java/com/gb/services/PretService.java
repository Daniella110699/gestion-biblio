package com.gb.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gb.models.Pret;
import com.gb.repositories.PretRepository;


@Service
public class PretService {

	@Autowired  
	PretRepository pretRepository;
	
	//get all  
	public List<Pret> getAllPret()   {  
	List<Pret> prets = new ArrayList<Pret>();  
	pretRepository.findAll().forEach(pret1 -> prets.add(pret1));  
	return prets;  
	} 
	
	//get one
	public Pret getPretById(Long pretId)   {  
	return pretRepository.findById(pretId).get();  
	} 
	
	//save 
	public void save(Pret pret)  {  
	pretRepository.save(pret);  			
	} 
	
	//delete
	public void delete(Long pretId)   {  
	pretRepository.deleteById(pretId);  
	} 
	
	//update
	public Pret update(Pret pret, Long pretId)
    {
        Pret pr = pretRepository.findById(pretId).get();
 
        if (Objects.nonNull(pret.getLecteur().getNumLecteur()) && !"".equalsIgnoreCase(pret.getLecteur().getNumLecteur())) {
            pr.setLecteur(pret.getLecteur());
        }
        
        if (Objects.nonNull(pret.getLivre().getNumLivre()) && !"".equalsIgnoreCase(pret.getLivre().getNumLivre())) {
            pr.setLivre(pret.getLivre());
        }
        
        if (Objects.nonNull(pret.getDatePret()) && !"".equalsIgnoreCase(pret.getDatePret())) {
            pr.setDatePret(pret.getDatePret());
        }
 
        return pretRepository.save(pr);
    }
}
