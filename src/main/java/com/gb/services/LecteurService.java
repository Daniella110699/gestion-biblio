package com.gb.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gb.models.Lecteur;
import com.gb.repositories.LecteurRepository;

@Service
public class LecteurService {

	@Autowired  
	LecteurRepository lecteurRepository;
	
	//get all  
	public List<Lecteur> getAllLecteur()   {  
	List<Lecteur> lecteurs = new ArrayList<Lecteur>();  
	lecteurRepository.findAll().forEach(lecteur1 -> lecteurs.add(lecteur1));  
	return lecteurs;  
	} 
	
	//get one
	public Lecteur getLecteurById(String numLecteur)   {  
	return lecteurRepository.findById(numLecteur).get();  
	} 
	
	//save 
	public void save(Lecteur lecteur)  {  
	lecteurRepository.save(lecteur);  			
	} 
	
	//delete
	public void delete(String numLecteur)   {  
	lecteurRepository.deleteById(numLecteur);  
	} 
	
	//update
	public Lecteur update(Lecteur lecteur, String numLecteur)
    {
        Lecteur lec = lecteurRepository.findById(numLecteur).get();
 
        if (Objects.nonNull(lecteur.getNomLecteur()) && !"".equalsIgnoreCase(lecteur.getNomLecteur())) {
            lec.setNomLecteur(lecteur.getNomLecteur());
        }
 
        return lecteurRepository.save(lec);
    }
}
