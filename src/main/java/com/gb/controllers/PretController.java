package com.gb.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.gb.models.Pret;
import com.gb.services.PretService;

@CrossOrigin("*")
@RestController
public class PretController {

	@Autowired  
	PretService pretService;
	
	//get all
	@GetMapping("/pret/get")  
	private List<Pret> getAllPret()   
	{  
	return pretService.getAllPret();  
	}
	
	//get one
	@GetMapping("/pret/{pretId}")  
	private Pret getPret(@PathVariable("pretId") Long pretId)   
	{  
	return pretService.getPretById(pretId);  
	} 
	
	//delete
	@DeleteMapping("/pret/delete/{pretId}")  
	private void deletePret(@PathVariable("pretId") Long pretId)   
	{  
	pretService.delete(pretId);  
	} 
	
	//save 
	@PostMapping("/pret/save")  
	private Long savePret(@RequestBody Pret pret)   
	{  
	pretService.save(pret);  
	return pret.getPretId();  
	}  
	
	//update  
	@PutMapping("/pret/update/{pretId}")  
	private Pret updatePret(@RequestBody Pret pret, @PathVariable("pretId") Long pretId)   
	{   
	return pretService.update(pret, pretId);  
	}  
}
