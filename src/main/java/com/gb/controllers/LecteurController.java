package com.gb.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.gb.models.Lecteur;
import com.gb.services.LecteurService;

@CrossOrigin("*")
@RestController
public class LecteurController {
  
	@Autowired  
	LecteurService lecteurService;
	
	//get all
	@GetMapping("/lecteur/get")  
	private List<Lecteur> getAllLecteur()   
	{  
	return lecteurService.getAllLecteur();  
	}
	
	//get one
	@GetMapping("/lecteur/{numLecteur}")  
	private Lecteur getLecteur(@PathVariable("numLecteur") String numLecteur)   
	{  
	return lecteurService.getLecteurById(numLecteur);  
	} 
	
	//delete
	@DeleteMapping("/lecteur/delete/{numLecteur}")  
	private void deleteLecteur(@PathVariable("numLecteur") String numLecteur)   
	{  
	lecteurService.delete(numLecteur);  
	} 
	
	//save 
	@PostMapping("/lecteur/save")  
	private String saveLecteur(@RequestBody Lecteur lecteur)   
	{  
	lecteurService.save(lecteur);  
	return lecteur.getNumLecteur();  
	}  
	
	//update  
	@PutMapping("/lecteur/update/{numLecteur}")  
	private Lecteur updateLecteur(@RequestBody Lecteur lecteur, @PathVariable("numLecteur") String numLecteur)   
	{   
	return lecteurService.update(lecteur, numLecteur);  
	}  
}
