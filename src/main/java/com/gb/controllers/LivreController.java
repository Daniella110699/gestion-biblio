package com.gb.controllers;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.gb.models.Livre;
import com.gb.services.LivreService;

@CrossOrigin("*")
@RestController
public class LivreController {

	@Autowired  
	LivreService livreService;
	
	//creating a get mapping that retrieves all the books detail from the database   
	@GetMapping("/livre/get")  
	private List<Livre> getAllLivre()   
	{  
	return livreService.getAllLivre();  
	}
	
	//creating a get mapping that retrieves the detail of a specific book  
	@GetMapping("/livre/{numLivre}")  
	private Livre getLivre(@PathVariable("numLivre") String numLivre)   
	{  
	return livreService.getLivreById(numLivre);  
	} 
	
	//creating a delete mapping that deletes a specified book  
	@DeleteMapping("/livre/delete/{numLivre}")  
	private void deleteLivre(@PathVariable("numLivre") String numLivre)   
	{  
	livreService.delete(numLivre);  
	} 
	
	//creating post mapping that post the book detail in the database  
	@PostMapping("/livre/save")  
	private String saveLivre(@RequestBody Livre livre)   
	{  
	livreService.save(livre);  
	return livre.getNumLivre();  
	}  
	
	//update  
	@PutMapping("/livre/update/{numLivre}")  
	private Livre updateLivre(@RequestBody Livre livre, @PathVariable("numLivre") String numLivre)   
	{   
	return livreService.update(livre, numLivre);
	}  
}
