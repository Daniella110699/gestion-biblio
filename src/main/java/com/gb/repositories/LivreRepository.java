package com.gb.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gb.models.Livre;

public interface LivreRepository extends JpaRepository<Livre, String>{

}
