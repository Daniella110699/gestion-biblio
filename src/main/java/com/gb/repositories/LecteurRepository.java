package com.gb.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gb.models.Lecteur;

public interface LecteurRepository extends JpaRepository<Lecteur, String>{

}
