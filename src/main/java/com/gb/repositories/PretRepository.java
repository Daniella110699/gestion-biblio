package com.gb.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gb.models.Pret;

@Repository
public interface PretRepository extends JpaRepository<Pret, Long>{

}
