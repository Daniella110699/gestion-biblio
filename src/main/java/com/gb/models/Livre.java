package com.gb.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
public class Livre implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(generator = "liv-generator")
    @GenericGenerator(name = "liv-generator", 
    parameters = @Parameter(name = "prefix", value = "LIV"), 
    strategy = "com.gb.models.IdGenerator")
	private String numLivre;
	
	private String designation;
	private String auteur;
	private String dateEdition;
	
	private String disponible;
	public String getNumLivre() {
		return numLivre;
	}
	public void setNumLivre(String numLivre) {
		this.numLivre = numLivre;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getAuteur() {
		return auteur;
	}
	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}
	public String getDateEdition() {
		return dateEdition;
	}
	public void setDateEdition(String dateEdition) {
		this.dateEdition = dateEdition;
	}
	public String getDisponible() {
		return disponible;
	}
	public void setDisponible(String disponible) {
		this.disponible = disponible;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public Livre() {
		super();
	}
	public Livre(String numLivre, String designation, String auteur, String dateEdition, String disponible) {
		super();
		this.numLivre = numLivre;
		this.designation = designation;
		this.auteur = auteur;
		this.dateEdition = dateEdition;
		this.disponible = disponible;
	}
	
}
