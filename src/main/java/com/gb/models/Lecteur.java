package com.gb.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
public class Lecteur implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(generator = "lec-generator")
    @GenericGenerator(name = "lec-generator", 
    parameters = @Parameter(name = "prefix", value = "LEC"), 
    strategy = "com.gb.models.IdGenerator")
	private String numLecteur;
	private String nomLecteur;
	
	public String getNumLecteur() {
		return numLecteur;
	}
	public void setNumLecteur(String numLecteur) {
		this.numLecteur = numLecteur;
	}
	public String getNomLecteur() {
		return nomLecteur;
	}
	public void setNomLecteur(String nomLecteur) {
		this.nomLecteur = nomLecteur;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Lecteur() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Lecteur(String numLecteur, String nomLecteur) {
		super();
		this.numLecteur = numLecteur;
		this.nomLecteur = nomLecteur;
	}

	
}
