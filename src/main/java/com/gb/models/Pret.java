package com.gb.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class Pret implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long pretId;
	
	private String datePret;
	
	@ManyToOne
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name ="numLecteur")
	private Lecteur lecteur;
	
	@ManyToOne
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name ="numLivre")
	private Livre livre;

	public Long getPretId() {
		return pretId;
	}

	public void setPretId(Long pretId) {
		this.pretId = pretId;
	}

	public String getDatePret() {
		return datePret;
	}

	public void setDatePret(String datePret) {
		this.datePret = datePret;
	}

	public Lecteur getLecteur() {
		return lecteur;
	}

	public void setLecteur(Lecteur lecteur) {
		this.lecteur = lecteur;
	}

	public Livre getLivre() {
		return livre;
	}

	public void setLivre(Livre livre) {
		this.livre = livre;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Pret() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Pret(Long pretId, String datePret, Lecteur lecteur, Livre livre) {
		super();
		this.pretId = pretId;
		this.datePret = datePret;
		this.lecteur = lecteur;
		this.livre = livre;
	}
	
}
